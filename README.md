# Loganalyzer
A library for analyzing http log files.

#### Notes for the reviewer
Code in bin/* and lib/printer/* was written for fun.  
Didn't write tests as I was unsure if I would keep it.  
I wouldn't use so much dynamic dispatching in a **large** production app.

### Install
Install dependencies with `bundle`

### Test
Run test suite with `rake spec`.  
Do a `rake -T` for more code quality goodies.
## Usage
   
   > ruby bin/analyze.rb <logfile path> [--unique | -all] [--text | --html]

#### Examples

```shell
ruby bin/analyze.rb ./webserver.log --unique --text
ruby bin/analyze.rb ./webserver.log --all --html

# or without options to use defaults: --unique, --text

ruby bin/analyze.rb ./webserver.log
```
