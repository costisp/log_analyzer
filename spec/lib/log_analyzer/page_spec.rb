# frozen_string_literal: true

describe LogAnalyzer::Page do
  let(:ip_addr_a) { '192.168.0.1' }
  let(:ip_addr_b) { '192.168.0.2' }
  let(:ip_addr_c) { '192.168.0.3' }
  let(:path) { 'springtime' }

  subject { described_class.new(path: path) }

  it 'initializes with given path' do
    expect(subject.path).to eq(path)
  end

  describe '#path' do
    it 'returns the path' do
      expect(subject.path).to eq path
    end
  end

  describe '#add_visit' do
    context 'when passing the same IP address' do
      it 'increases the total visit count' do
        subject.add_visit(ip_addr: ip_addr_a)
        subject.add_visit(ip_addr: ip_addr_a)
        expect(subject.total_visits).to eq 2
      end

      it 'does not increase unique visit count' do
        subject.add_visit(ip_addr: ip_addr_a)
        subject.add_visit(ip_addr: ip_addr_a)
        expect(subject.unique_visits).to eq 1
      end
    end

    context 'when passing different IP addresses' do
      it 'increases the total visit count' do
        subject.add_visit(ip_addr: ip_addr_a)
        subject.add_visit(ip_addr: ip_addr_b)
        expect(subject.total_visits).to eq 2
      end
    end

    context 'with a mix of IP addresses' do
      it 'keeps correct count of unique and total visits' do
        subject.add_visit(ip_addr: ip_addr_a)
        subject.add_visit(ip_addr: ip_addr_a)
        subject.add_visit(ip_addr: ip_addr_b)
        subject.add_visit(ip_addr: ip_addr_c)
        subject.add_visit(ip_addr: ip_addr_c)

        expect(subject.total_visits).to eq 5
        expect(subject.unique_visits).to eq 3
      end
    end
  end
end
