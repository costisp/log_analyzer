# frozen_string_literal: true

describe LogAnalyzer::Pages do
  subject { LogAnalyzer::Pages.new }

  describe('#initialize') do
    it 'initializes an empty collection' do
      expect(subject.total_visits).to eq 0
    end
  end

  describe('#add_visit') do
    it 'works like a charm' do
      subject.add_visit(path: '/foo', ip_addr: '192.168.0.1')
      expect(subject.total_visits).to eq 1
    end
  end

  describe('#unique_visits') do
    it 'works like a charm 2' do
      subject.add_visit(path: '/foo', ip_addr: '192.168.0.1')
      subject.add_visit(path: '/foo', ip_addr: '192.168.0.1')
      subject.add_visit(path: '/bar', ip_addr: '192.168.0.2')
      subject.add_visit(path: '/bar', ip_addr: '192.168.0.2')
      subject.add_visit(path: '/xip', ip_addr: '192.168.0.3')

      expect(subject.total_visits).to eq 5
      expect(subject.unique_visits).to eq 3
    end
  end

  describe('sorting by unique and total visits') do
    before do
      subject.add_visit(path: '/foo', ip_addr: '192.168.0.1')
      subject.add_visit(path: '/bar', ip_addr: '192.168.0.1')
      subject.add_visit(path: '/bar', ip_addr: '192.168.0.2')
      subject.add_visit(path: '/bar', ip_addr: '192.168.0.3')
      subject.add_visit(path: '/xip', ip_addr: '192.168.0.1')
      subject.add_visit(path: '/xip', ip_addr: '192.168.0.2')
      subject.add_visit(path: '/xip', ip_addr: '192.168.0.3')
      subject.add_visit(path: '/xip', ip_addr: '192.168.0.4')
    end

    it 'does the total' do
      expect(subject.order_by_total_visits.map do |request_path|
        [request_path.path, request_path.total_visits]
      end).to eq [['/foo', 1], ['/bar', 3], ['/xip', 4]]
    end

    it 'does the uniques' do
      expect(subject.order_by_unique_visits.map do |request_path|
        [request_path.path, request_path.unique_visits]
      end).to eq [['/foo', 1], ['/bar', 3], ['/xip', 4]]
    end
  end
end
