#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/log_analyzer'

file_path = ARGV.shift

unless file_path && file_path.strip != ''
  puts "Usage: #{$PROGRAM_NAME} <filepath> [--uniques | --all] [--text | --html]"
  exit 1
end

abort "File not found #{ARGV[0]}" unless File.exist?(file_path.strip)

# Parse first arg: the sort order option.
# Valid options are --unique or --all. Defaults to '--unique'
sorter = case ARGV.shift
         when '--unique'
           :unique_visits
         when '--all'
           :total_visits
         else
           :unique_visits
         end

# Parse next arg: the output format option.
# Valid options are --text or --html. Defaults to '--text'
output_format = case ARGV.shift
                when '--text'
                  :text
                when '--html'
                  :html
                else
                  :text
                end

# Open the logfile and populate a Pages collection.
pages = LogAnalyzer::Pages.new
LogParser.with_file(file_path: file_path) do |path, ip_addr|
  pages.add_visit(path: path, ip_addr: ip_addr)
end

# Sort Pages collection by
sorted_pages = pages.send("order_by_#{sorter}".to_sym)

# Locate a Printer and print the collection
LogAnalyzer::Printers
  .for_type(output_format)
  .print(pages: sorted_pages, prop: sorter.to_sym)
