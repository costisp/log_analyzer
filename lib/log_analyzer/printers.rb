# frozen_string_literal: true

module LogAnalyzer
  # 'Printers' are objects that know how to print Page collections.
  # Each Printer supports a specific format like :text, :html etc
  # To locate a Printer class for the desired format do:
  #
  #     printer = LogAnalyzer::Printers.formatter_for(:text)
  #
  # then send a Page collection for printing:
  #
  #     printer.print(pages)
  #
  module Printers
    @printers = []

    class << self
      # Add a Printer to the list of available Printers
      def <<(formatter)
        @printers << formatter
      end

      # Locates a Printer class that supports format of 'type'
      def for_type(type)
        unless %i[text html].include?(type.to_sym)
          raise ArgumentError 'Invalid formatter type'
        end

        printers.find { |formatter| formatter.for_type == type.to_sym }
      end

      private

      attr_reader :printers
    end
  end

end

require_relative 'printers/base'
require_relative 'printers/html'
require_relative 'printers/text'
