# frozen_string_literal: true

module LogAnalyzer
  # This class represents a collection of Page objects
  # It can sort pages by unique visit count or by total visits.
  #
  # = Example usage
  #     pages = Pages.new
  #     pages.add_request('some_path', 'ip_addr')
  #     pages.order_by_total_visits
  class Pages
    def initialize
      @pages = Hash.new { |hash, key| hash[key] = Page.new(path: key) }
    end

    # adds a request to a page
    # if page does not exist a new one will be created
    def add_visit(path:, ip_addr:)
      @pages[path].add_visit(ip_addr: ip_addr)
    end

    # return number of total visits in this page collection
    def total_visits
      return 0 unless @pages.any?

      @pages.values.map(&:total_visits).reduce(:+)
    end

    # return number of unique visits in this page collection
    def unique_visits
      return 0 unless @pages.any?

      @pages.values.map(&:unique_visits).reduce(:+)
    end

    # return this page collection sorted by total visits
    def order_by_total_visits
      order_by(:total_visits)
    end

    # return this page collection sorted by unique visits
    def order_by_unique_visits
      order_by(:unique_visits)
    end

    private

    attr_accessor :pages

    def order_by(property)
      valid_args = %i[total_visits unique_visits]
      raise ArgumentError 'not valid' unless valid_args.include?(property)

      @pages.values.sort_by(&property)
    end
  end
end
