# frozen_string_literal: true

module LogAnalyzer
  module Printers
    # A formatter for HTML output
    class Html < Base
      def self.for_type
        :html
      end

      private

      def header
        puts '<html><body>'
        puts "<h1>Pages sorted by #{@_prop}"
        puts '<table>'
        puts '<tr><th>Page path</th><th>Count</th></tr>'
      end

      def row(path)
        puts "<tr><td>#{path.path}</td><td> #{path.send(@_prop)}</td></tr>"
      end

      def footer
        puts '</table>'
        puts '</body></html>'
      end
    end
  end
end
