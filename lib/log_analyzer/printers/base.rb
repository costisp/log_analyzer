# frozen_string_literal: true

module LogAnalyzer
  module Printers
    # Abstract class, implements printing algorithm.
    class Base
      def self.inherited(subclass)
        Printers << subclass
      end

      # @param pages <Pages[]> a collection of pages
      # @param prop <Symbol> The property to be used in the report
      #                      Can be either total_visits or unique_visits
      def self.print(pages:, prop:)
        new(pages: pages, prop: prop).print
      end

      # The output type this Printer supports. Implemented by subclasses.
      def self.for_type
        raise NotImplementedError 'to be implemented by subclasses'
      end

      # @param pages <Pages[]> a collection of pages .
      # @param prop <Symbol> The property to be used in the report.
      def initialize(pages:, prop:)
        @_pages = pages
        @_prop  = prop
      end

      # the printing algorithm
      def print
        header
        @_pages.each { |path| row(path) }
        footer
      end

      private

      # The header part of the report
      def header
        raise NotImplementedError 'to be implemented by subclasses'
      end

      # The row
      def row(_path)
        raise NotImplementedError 'to be implemented by subclasses'
      end

      # The footer part of the report
      def footer
        raise NotImplementedError 'to be implemented by subclasses'
      end
    end
  end
end
