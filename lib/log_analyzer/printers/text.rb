# frozen_string_literal: true

require_relative 'base'

module LogAnalyzer
  module Printers
    # A formatter for text output.
    class Text < Base
      def self.for_type
        :text
      end

      private

      def header
        puts '*' * 60
        puts "Pages sorted by #{@_prop}"
        puts '*' * 60
      end

      def row(path)
        puts "#{path.path} => #{path.send(@_prop)}"
      end

      def footer; end
    end
  end
end
