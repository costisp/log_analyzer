# frozen_string_literal: true

module LogAnalyzer
  # Object representing a Page.
  # A Page is identified by its path.
  # Keeps track of total and unique page visits.
  class Page
    attr_reader :path

    def initialize(path:)
      @path   = path
      @visits = Hash.new { |hash, key| hash[key] = 0 }
    end

    def add_visit(ip_addr:)
      @visits[ip_addr] += 1
    end

    def total_visits
      @visits.values.reduce(:+)
    end

    def unique_visits
      @visits.length
    end

    private

    attr_accessor :visits
  end
end
