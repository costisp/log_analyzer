# frozen_string_literal: true

# This class reads a logfile and iterates over each log line
# TODO: this needs work ASAP. We are not closing the file handle.
class LogParser
  def self.with_file(file_path:, opts: { separator: ' ' }, &block)
    new(file_path, opts).each(&block)
  end

  def initialize(file_path, opts)
    @_separator = opts.fetch(:separator, ' ')
    @_file = File.open(file_path)
  end

  def each
    @_file.each_line do |line|
      path, ip_addr = line.split(@_separator)
      yield(path, ip_addr)
    end
  end
end
