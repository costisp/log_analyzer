# frozen_string_literal: true

require_relative 'log_analyzer/page'
require_relative 'log_analyzer/pages'
require_relative 'log_analyzer/log_parser'
require_relative 'log_analyzer/printers'

# A library for analyzing log files
# Example usage:
#
#    pages = LogAnalyzer::Pages.new
#    pages.add_visit('/about', '131.45.22.55')
#    pages.add_visit('/contact', '131.45.22.56')
#    LogAnalyzer::Printers
#        .for_type(:text)
#        .print(pages: pages.order_by_unique_visits, prop: sorter.to_sym)
module LogAnalyzer
end
